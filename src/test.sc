val b = 10 match {
  case _ => {
    11
  }
  case 10 => {
    println("Hello!")
    "10"
  }
}

val c: Any = "string"

c match {
  case "string" | "otherstring" => "exact match"
  case c: String if c == "string"|| c == "otherstring" => "type match, does the same as the previous case"
  case i: Int => "won't match, because c is a string"
  case everything @ _ => print(everything)
}

// ** //

val list = List(1, 2, 3, 4, 5, 6, 7, 8, 100500)
def printList(list: List[Int]): Unit = list match {
  case head :: Nil => println(head)
  case head :: tail =>
    printList(tail)
    println(head)
}

printList(list)

//

val list1 = List("3", 2, 5)
list1.collect {
  case i: Int => i
}
