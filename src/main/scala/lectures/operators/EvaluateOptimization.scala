package lectures.operators

import lectures.functions.CurriedComputation
import lectures.functions.Data
import lectures.functions.Computation._

/**
  * В задачке из lectures.functions.Computations, мы реализовали
  * один и тот же метод 3-я разными способами
  *
  * Пришло время оценить на сколько разные имплементации
  * отличаются друг от друга по производительности
  *
  * Для этого, раскомментируйте код, выполните в циклах вызов 3-х имплементаций
  * Оцените разницу во времени выполнения и объясните ее происхожение
  *
  */
object EvaluateOptimization extends App with Data {

  val startTimestamp = System.currentTimeMillis()


  // ВЫПОЛНИТЬ В ЦИКЛЕ  ОТ 1 ДО 100 Computation.computation(

  // 11294
//  for(i <- 1 to 100) {
//    computation(filterData, dataArray)
//    println("elapsed time in Computation.computation" + (System.currentTimeMillis() - startTimestamp))
//  }

  // ВЫПОЛНИТЬ В ЦИКЛЕ  ОТ 1 ДО 100 CurriedComputation.partiallyAppliedCurriedFunction(
  for(i <- 1 to 100) {
    CurriedComputation.partiallyAppliedCurriedFunction(dataArray)
    print("elapsed time " + (System.currentTimeMillis() - startTimestamp))
  }

  // ВЫПОЛНИТЬ В ЦИКЛЕ  ОТ 1 ДО 100 FunctionalComputation.filterApplied
  //    for(??? <- ???) {
  //     ???
  //    print("elapsed time " + (System.currentTimeMillis() - startTimestamp))
  //   }

  // ВЫВЕСТИ РАЗНИЦУ В ПРОДОЛЖИТЕЛЬНОСТИ ВЫПОЛНЕНИЯ МЕЖДУ КАРРИРОВАННОЙ ВЕРСИЕЙ
  // И ФУНКЦИОНАЛЬНОЙ

  val diff = System.currentTimeMillis() - startTimestamp;

  print(s"Difference is about $diff milliseconds")
}

